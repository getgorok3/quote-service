package main

import (
	"quote-service/api/router"

	"github.com/kataras/iris"
)

// start Web server
func main() {
	app := router.App
	router.RestRouter(app)

	app.Run(iris.Addr(":5000"))
}
