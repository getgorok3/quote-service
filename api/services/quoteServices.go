package services

import (
	"context"
	"quote-service/api"
	"quote-service/api/model"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

//CreateQuote insert data to db
func CreateQuote(inputQuote model.Quote) (resultID string, err error) {

	result, err := inputQuote.Create()
	if err != nil {
		return
	}

	resultID = result
	return resultID, nil
}

//GetQuoteDetail get quote information
func GetQuoteDetail(resID string) (result model.Quote, err error) {
	col := api.ConnectDB()

	var quote model.Quote

	filterID, _ := primitive.ObjectIDFromHex(resID)
	filter := bson.M{"_id": filterID}
	err = col.FindOne(context.Background(), filter).Decode(&quote)

	if err != nil {
		return model.Quote{}, err
	}

	return quote, nil
}

//EditQuote edit quote detail
func EditQuote(inputQuote model.Quote) (resultID string, err error) {
	col := api.ConnectDB()

	filter := bson.M{"_id": inputQuote.ID}

	update := bson.M{
		"$set": bson.M{
			"description": inputQuote.Description,
			"url":         inputQuote.URL,
			"color":       inputQuote.Color,
		},
	}

	_, err = col.UpdateOne(context.TODO(), filter, update, options.Update().SetUpsert(true))
	if err != nil {
		return "0", err
	}
	resultID = inputQuote.ID.Hex()
	return resultID, nil
}

//DeleteQuote delete a specific quote
func DeleteQuote(resID string) (deleteID string, err error) {
	col := api.ConnectDB()

	filterID, _ := primitive.ObjectIDFromHex(resID)
	filter := bson.M{"_id": filterID}
	_, err = col.DeleteOne(context.TODO(), filter)
	if err != nil {
		return "0", err
	}
	deleteID = resID
	return deleteID, nil
}

//FilterQuote get all qoutes
func FilterQuote(inputfilter model.RequestQuery) (result []model.Quote, total int64, err error) {
	col := api.ConnectDB()
	quotes := []model.Quote{}

	// check filter values
	var requestQuery model.RequestQuery
	if inputfilter.Limit != 0 {
		requestQuery.Limit = inputfilter.Limit
	}

	if inputfilter.Description != "" {
		requestQuery.Description = inputfilter.Description
	}

	if inputfilter.Page != 0 {
		requestQuery.Page = inputfilter.Page
	}

	// paging
	//amount of quote that need to skip
	skipTo := (requestQuery.Page - 1) * 10
	findOptions := options.Find()

	findOptions.SetLimit(requestQuery.Limit)
	findOptions.SetSkip(skipTo)

	// filter by character in quote's name: I using regex to help searching operation
	filter := bson.M{"description": bson.M{"$regex": requestQuery.Description}}
	res, err := col.Find(context.TODO(), filter, findOptions)
	if err != nil {
		return []model.Quote{}, 0, err
	}

	for res.Next(context.TODO()) {
		var eachQuote model.Quote
		err := res.Decode(&eachQuote)
		if err != nil {
			return []model.Quote{}, 0, err
		}
		quotes = append(quotes, eachQuote)
	}
	res.Close(context.TODO())

	// sum total quote in db
	total, err = col.CountDocuments(context.TODO(), bson.M{}, nil)
	if err != nil {
		return []model.Quote{}, 0, err
	}

	return quotes, total, nil
}
