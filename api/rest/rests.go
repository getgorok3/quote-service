package rest

import (
	"quote-service/api/constants"
	"quote-service/api/model"
	"quote-service/api/services"
	"strconv"

	"github.com/kataras/iris"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//PostQuote add new quote
func PostQuote(ctx iris.Context) {
	var inputQuote model.Quote
	//handle invalid param
	if err := ctx.ReadJSON(&inputQuote); err != nil {
		constants.GetErrorResponse(ctx, 400)
		return
	}

	resultID, resultErr := services.CreateQuote(inputQuote)
	if resultErr != nil {
		constants.GetErrorResponse(ctx, 400)
		return
	}

	constants.GetSuccessResponse(ctx, resultID)
	return
}

//GetQuote get quote details
func GetQuote(ctx iris.Context) {

	quoteID := ctx.Params().Get("id")
	// handle invalid id
	if len(quoteID) < 24 {
		constants.GetErrorResponse(ctx, 400)
		return
	}

	result, err := services.GetQuoteDetail(quoteID)

	//handle object not found
	if err != nil {
		constants.GetErrorResponse(ctx, 404)
		return
	}

	constants.GetSuccessResponse(ctx, result)
	return
}

//PutQuote edit new quote
func PutQuote(ctx iris.Context) {
	var inputQuote model.Quote
	//handle invalid param
	if err := ctx.ReadJSON(&inputQuote); err != nil {

		constants.GetErrorResponse(ctx, 400)
		return
	}

	quoteID := ctx.Params().Get("id")
	// handle invalid id
	if len(quoteID) < 24 {
		constants.GetErrorResponse(ctx, 400)
		return
	}
	// add id from url
	inputQuote.ID, _ = primitive.ObjectIDFromHex(quoteID)

	resultID, resultErr := services.EditQuote(inputQuote)
	if resultErr != nil {

		constants.GetErrorResponse(ctx, 400)
		return
	}

	constants.GetSuccessResponse(ctx, resultID)
	return
}

//DeleteQuote delete specific quote
func DeleteQuote(ctx iris.Context) {

	quoteID := ctx.Params().Get("id")
	// handle invalid id
	if len(quoteID) < 24 {
		constants.GetErrorResponse(ctx, 400)
		return
	}

	result, err := services.DeleteQuote(quoteID)

	//handle object not found
	if err != nil {
		constants.GetErrorResponse(ctx, 404)
		return
	}

	constants.GetSuccessResponse(ctx, result)
	return
}

//GetAllQuotes get all the qoutes in db
func GetAllQuotes(ctx iris.Context) {
	page, _ := strconv.ParseInt(ctx.URLParamDefault("page", "1"), 10, 64)
	limit, _ := strconv.ParseInt(ctx.URLParamDefault("limit", "10"), 10, 64)
	name := ctx.URLParamDefault("description", "")

	requestQuery := model.RequestQuery{
		Limit:       limit,
		Page:        page,
		Description: name,
	}

	result, total, resultErr := services.FilterQuote(requestQuery)
	if resultErr != nil {
		constants.GetErrorResponse(ctx, 400)
		return
	}

	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(iris.Map{
		"total": total,
		"data":  result,
		"status": iris.Map{
			// "total":   total,
			// "data":    result,
			"code":    200,
			"message": constants.Success,
		},
	})
	return
}
