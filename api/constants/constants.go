package constants

import "github.com/kataras/iris"

const (
	//InvalidJSON error message
	InvalidJSON = "Invalid Json"

	//InvalidParam error message
	InvalidParam = "Invalid Param"

	//Success  message
	Success = "Success"

	//NotFound  message
	NotFound = "NotFound"

	//CannotEdit  message
	CannotEdit = "Can not edit quote"

	//CannotDelete  message
	CannotDelete = "Can not delete quote"
)

//GetErrorResponse return error response
func GetErrorResponse(ctx iris.Context, code int) {
	var errMessage string
	if code == 404 {
		ctx.StatusCode(iris.StatusNotFound)
		errMessage = NotFound
	} else {
		ctx.StatusCode(iris.StatusBadRequest)
		errMessage = InvalidJSON
	}
	ctx.JSON(iris.Map{
		"status": iris.Map{
			"code":    code,
			"message": errMessage,
		},
	})
}

//GetSuccessResponse return normal success response
func GetSuccessResponse(ctx iris.Context, result interface{}) {
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(iris.Map{
		"data": result,
		"status": iris.Map{
			"code":    200,
			"message": Success,
		},
	})
}
