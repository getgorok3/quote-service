package model

import (
	"context"
	"errors"
	"quote-service/api"
	"quote-service/api/constants"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/go-playground/validator.v9"
)

//Quote good sentences for inspiration
type Quote struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Description string             `json:"description" bson:"description,"`
	URL         string             `json:"url" bson:"url,"`
	Color       string             `json:"color" bson:"color,"`
	// Author      *Author            `json:"author" bson:"author,omitempty"`
}

// //Author who wrote the quote
// type Author struct {
// 	Name string `json:"name,omitempty" bson:"name"`
// }

//RequestQuery is query object
type RequestQuery struct {
	Limit       int64  `json:"limit,omitempty" bson:"limit,omitempty"`
	Page        int64  `json:"page,omitempty" bson:"page,omitempty"`
	Description string `json:"description,omitempty" bson:"description,omitempty"`
}

//Create insert quote into db
func (quote *Quote) Create() (id string, err error) {
	col := api.ConnectDB()

	validate := validator.New()
	err = validate.Struct(quote)
	if err != nil {
		return "0", errors.New(constants.InvalidJSON)
	}

	result, err := col.InsertOne(context.TODO(), quote)
	if err != nil {
		return "0", errors.New(constants.InvalidJSON)
	}
	newID := result.InsertedID.(primitive.ObjectID)
	id = newID.Hex()
	return id, nil
}
