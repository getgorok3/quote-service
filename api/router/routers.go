package router

import (
	"quote-service/api/rest"

	"github.com/kataras/iris"
	"github.com/kataras/iris/core/router"
)

var (
	//App .
	App = iris.Default()
)

//RestRouter set all path url :: Group All API path and map with function
func RestRouter(irisApp *iris.Application) router.Party {

	cors := irisApp.Party("/quote").AllowMethods(iris.MethodOptions)
	{
		cors.Handle("POST", "/add", rest.PostQuote)
		cors.Handle("GET", "/{id:string}", rest.GetQuote)
		cors.Handle("PUT", "/{id:string}", rest.PutQuote)
		cors.Handle("DELETE", "/{id:string}", rest.DeleteQuote)
		cors.Handle("GET", "/all", rest.GetAllQuotes)

	}

	return cors
}
